#!/bin/bash
set -e -x

LD_LIBRARY_PATH="/flint-py/ext/arb:/flint-py/ext/flint:/usr/local/lib"

# Compile wheels
for PYBIN in /opt/python/*/bin; do
    "${PYBIN}/pip" install cython
    "${PYBIN}/python" setup.py bdist_wheel
    # Hack to re-trigger cython build for every python version
    # Important for Python 2.7 to build once with UCS2 and once with UCS4
    rm src/pyflint.c
done

mkdir wheelhouse

# Bundle external shared libraries into the wheels
for whl in dist/*.whl; do
    auditwheel repair "$whl" --wheel-dir wheelhouse
done
