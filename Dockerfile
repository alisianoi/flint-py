FROM quay.io/pypa/manylinux2010_x86_64 as builder

# Either install this or set MAKEINFO=true on make calls (if it works)
# RUN yum -y install texinfo && yum -y update; yum clean all

WORKDIR /flint-py

# Stage 1: configure and build all of the dependencies
ENV EXT /flint-py/ext
ADD ext ${EXT}

WORKDIR ${EXT}/mpir
RUN ./autogen.sh && ./configure --enable-gmpcompat && make MAKEINFO=true -j4

WORKDIR ${EXT}/mpfr
RUN ./configure --with-gmp-include=${EXT}/mpir --with-gmp-lib=${EXT}/mpir/.libs && make -j4

WORKDIR /flint-py/ext/flint
RUN ./configure --with-mpir=${EXT}/mpir --with-mpfr=${EXT}/mpfr && make -j4

WORKDIR /flint-py/ext/arb
RUN ./configure --with-mpir=${EXT}/mpir --with-mpfr=${EXT}/mpfr --with-flint=${EXT}/flint && make -j4

# Stage 2: build the python wrapper
ADD src /flint-py/src
ADD test /flint-py/test

ADD setup.py /flint-py/setup.py
ADD README.md /flint-py/README.md
ADD LICENSE /flint-py/LICENSE
ADD MANIFEST.in /flint-py/MANIFEST.in

WORKDIR /flint-py
ADD build_wheels.sh /flint-py
RUN ./build_wheels.sh
