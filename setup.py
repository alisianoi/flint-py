import os
import sys

from setuptools import setup
from setuptools.extension import Extension


ext_modules = [
    Extension(
        "flint", ["src/pyflint.pyx"],
        libraries=["flint", "arb"],
        include_dirs=["ext", "ext/mpir",       "ext/mpfr/src",       "ext/arb", "ext/flint2"],
        library_dirs=["ext", "ext/mpir/.libs", "ext/mpfr/src/.libs", "ext/arb", "ext/flint2"],
    )
]

for e in ext_modules:
    e.cython_directives = {"embedsignature": True}

with open("README.md") as readme:
    long_description = readme.read()

setup(
    name='flint-py',
    ext_modules=ext_modules,
    description='Repackage python-flint to include arb and flint as shared libraries',
    long_description=long_description,
    long_description_content_type='text/markdown',
    version='0.3.7',
    url='https://gitlab.com/alisianoi/flint-py',
    author='Fredrik Johansson',
    author_email='fredrik.johansson@gmail.com',
    license='MIT',
    classifiers=['Topic :: Scientific/Engineering :: Mathematics']
)
